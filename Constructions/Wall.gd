extends Node

class_name Wall

@export var requirements = {"Wood" : 20}
var requirementsReady = {}
var requirementsOnTheWay = {}

@export var tileMapIndex : int
@export var tileMapPos : Vector2i
@export var placingMode : UI.PlacingMode
@export var buildDifficulty : float = 1

var buildProgress : float = 0

var position : Vector2

var terrain = null

func GetTileMapPos() -> Vector2i:
	return tileMapPos

func InitRequirements():
	for requirement in requirements:
		requirementsOnTheWay[requirement] = 0
		requirementsReady[requirement] = 0
		
func GetRequirements():
	return requirements
		
func GetRequirement(requirementName : String):
	var amount = requirements[requirementName]
	amount -= requirementsOnTheWay[requirementName]
	amount -= requirementsReady[requirementName]
	
	#print(str(requirements[requirementName]) + " - " + str(requirementsReady[requirementName]) + " = " + str(amount) + "   " + str(requirementsOnTheWay[requirementName]))
	return [requirementName, amount]
	
func DoesNeedRequirement(requirementName : String) -> bool:
	if GetRequirement(requirementName)[1] > 0:
		return true
	else:
		return false
	
	
func HasAllRequirements() -> bool :
	var hasEverything = true
	for requirement in requirements:
		
		if GetRequirement(requirement)[1] > 0:
			hasEverything = false
	return hasEverything
	
	
func ReportRequirementOnTheWay(requirementName, amount):
	requirementsOnTheWay[requirementName] += amount
	
func CancelRequirementOnTheWay(requirementName, amount):
	requirementsOnTheWay[requirementName] -= amount
	
func DeliverRequirement(requirementName, amount):
	requirementsReady[requirementName] += amount
	#requirementsOnTheWay[requirementName] -= amount


func TryBuild(amount : float) -> bool:
	buildProgress += amount * 1/buildDifficulty
	
	if IsBuilt():
		terrain.OnConstructionComplete(self)
		return true
	else:
		return false
		
func IsBuilt() -> bool:
	return buildProgress >= 1
		
func GetCount():
	if IsBuilt():
		return ""
	else:
		var str = ""
		for requirement in requirements:
			str += requirement + ": " + str(requirementsReady[requirement]) + " / " + str(requirements[requirement]) + "\n"
		return str
		
func GetName():
	if IsBuilt():
		return name
	else:
		return name + "    " + str(int(buildProgress*100)) + "%"
		
