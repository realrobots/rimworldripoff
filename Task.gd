extends Node

class_name Task

enum TaskType {BaseTask, FindItem, WalkTo, WalkNextTo, Pickup, Eat, Manipulate, Harvest, Build, Haul, RequirementDelivery}
enum Orders {Cancel, Deconstruct, Mine, Chop, Harvest}

var taskName: String
var taskType: TaskType = TaskType.BaseTask

var subTasks = []
var currentSubTask : int = 0

var groupedTasks = null

var targetItem
var targetItemType
var targetItemAmount = 1

var lastAttemptTime = 0

func IsFinished() -> bool:
	return currentSubTask == len(subTasks)
	
func Finish():
	currentSubTask = len(subTasks)
	
func Restart():
	currentSubTask = 0
	lastAttemptTime = Time.get_ticks_msec()
	
func GetCurrentSubTask():
	return subTasks[currentSubTask]
	
func OnFinishSubTask():
	currentSubTask += 1
	
func GetTargetItemName() -> String:
	return GetSubTask(TaskType.FindItem).targetItemType
	
func GetDeliveryAmount() -> int:
	return GetSubTask(TaskType.RequirementDelivery).targetItemAmount
	
func GetSubTask(subTaskType : TaskType):
	for task in subTasks:
		if task.taskType == subTaskType:
			return task
	return null
	
func GetNextTaskInGroup():
	print("GET NEXT TASK IN GROUP")
	if len(groupedTasks) > 0:
		var nextTask = groupedTasks[0]
		groupedTasks.remove_at(0)
		nextTask.groupedTasks = groupedTasks
		return nextTask	
	
func GoBackToSubTask(subTaskType : TaskType):
	while currentSubTask >= 0:
		currentSubTask -= 1
		if GetCurrentSubTask().taskType == subTaskType:
			break
			
func SkipAheadToSubTask(subTaskType : TaskType):
	while not IsFinished():
		OnFinishSubTask()
		if GetCurrentSubTask().taskType == subTaskType:
			break
	
func GroupSimilarRequirementDeliveries(similarDeliveryTasks):
	var totalResourcesNeeded = 0
	for task in similarDeliveryTasks:
		totalResourcesNeeded += task.GetSubTask(TaskType.Pickup).targetItemAmount
		
	GetSubTask(TaskType.Pickup).targetItemAmount += totalResourcesNeeded
	groupedTasks = similarDeliveryTasks
	
func OnFoundItem(item):
	OnFinishSubTask()
	GetCurrentSubTask().targetItem = item

func OnReachedDestination():
	OnFinishSubTask()
	GetCurrentSubTask().targetItem = subTasks[currentSubTask - 1].targetItem
	
func InitDeliveryTask(target, requirement):
	taskType = TaskType.RequirementDelivery
	
	var subTask = Task.new()
	subTask.taskType = TaskType.FindItem
	subTask.targetItemType = requirement[0]
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.WalkTo
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.Pickup
	subTask.targetItemAmount = requirement[1]
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.WalkNextTo
	subTask.targetItem = target
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.RequirementDelivery
	subTask.targetItem = target
	subTask.targetItemAmount = requirement[1]
	subTasks.append(subTask)
	
	
	
func InitHarvestPlantTask(target):
	taskType = TaskType.Harvest
	
	var subTask = Task.new()
	subTask.taskType = TaskType.WalkTo
	subTask.targetItem = target
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.Harvest
	subTask.targetItem = target
	subTasks.append(subTask)
	
func InitBuildTask(target):
	var subTask = Task.new()
	subTask.taskType = TaskType.WalkNextTo
	subTask.targetItem = target
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.Build
	subTask.targetItem = target
	subTasks.append(subTask)
	
func InitFindAndEatFoodTask():
	taskName = "Find and eat some food"
	
	var subTask = Task.new()
	subTask.taskType = TaskType.FindItem
	subTask.targetItemType = ItemManager.ItemCategory.FOOD
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.WalkTo
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.Pickup
	subTasks.append(subTask)
	
	subTask = Task.new()
	subTask.taskType = TaskType.Eat
	subTasks.append(subTask)
