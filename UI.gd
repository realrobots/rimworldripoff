extends CanvasLayer

class_name UI

@onready var itemManager = $"../ItemManager"
@onready var taskManager = $"../TaskManager"

enum PlacingMode {SINGLE, ROW, RECTANGLE}
enum UIMode {NORMAL, PLACING, DRAGGING, ORDERING, ORDERDRAGGING}

@onready var terrain = $"../Terrain"
@onready var draw_layer = $"../UIDrawingLayer"

var currentUIMode : UIMode = UIMode.NORMAL
var currentOrder : Task.Orders = Task.Orders.Cancel
var placingPrototype = null

var startPlacingPos : Vector2 = Vector2.ZERO


var buttonHeight = 0.05

func _process(delta):
	
	if Input.is_action_just_pressed("click_cancel"):
		placingPrototype = null
		terrain.clear_layer(Terrain.TerrainLayer.UIGhosts)
		draw_layer.Clear()
		OnItemSelect(null)
		ChangeUIMode(UIMode.NORMAL)
		
	if currentUIMode == UIMode.NORMAL:
		if Input.is_action_just_pressed("click"):
			startPlacingPos = GetMousePositionToTerrainPos()
			
		if Input.is_action_just_released("click"):
			if Vector2i(startPlacingPos) == GetMousePositionToTerrainPos():
				var itemAtPos = terrain.GetItemAtPosition(Vector2i(startPlacingPos))
				OnItemSelect(itemAtPos)
				
			
	elif currentUIMode == UIMode.PLACING:
		DrawPrototypeAtCursor()
		if Input.is_action_just_pressed("click"):
			startPlacingPos = GetMousePositionToTerrainPos()
			ChangeUIMode(UIMode.DRAGGING)
			
	elif currentUIMode == UIMode.DRAGGING:
			DrawPrototypeInLine()	
			
			if Input.is_action_just_released("click"):
				PlaceConstructionOrders()
				ChangeUIMode(UIMode.PLACING)
				
	elif currentUIMode == UIMode.ORDERING:
		DrawCrosshairAtCursor()
		
		if Input.is_action_just_pressed("click"):
			startPlacingPos = GetMousePositionToTerrainPos()
			ChangeUIMode(UIMode.ORDERDRAGGING)
			
	elif currentUIMode == UIMode.ORDERDRAGGING:
		DrawOrderingRectangle()
		
		if Input.is_action_just_released("click"):
				PlaceOrders()
				draw_layer.Clear()
				ChangeUIMode(UIMode.ORDERING)
			
			
func ChangeUIMode(mode : UIMode):
	currentUIMode = mode
	
func OnItemSelect(item):
	get_node("pnl_selected_item").SelectItem(item)
	if item != null:
		DrawCrosshairAtCursor()
	
func PlaceConstructionOrders():
	print("build " + placingPrototype.name)
	print(terrain.get_used_cells(Terrain.TerrainLayer.UIGhosts))
	for pos in terrain.get_used_cells(Terrain.TerrainLayer.UIGhosts):
		terrain.PlaceConstructionOrder(placingPrototype, pos)
	
	terrain.clear_layer(Terrain.TerrainLayer.UIGhosts)
	
		
		
func DrawPrototypeAtCursor():
	terrain.clear_layer(Terrain.TerrainLayer.UIGhosts)
	terrain.set_cell(Terrain.TerrainLayer.UIGhosts, GetMousePositionToTerrainPos(), placingPrototype.tileMapIndex, placingPrototype.tileMapPos)
	
func DrawCrosshairAtCursor():
	terrain.clear_layer(Terrain.TerrainLayer.UIGhosts)
	terrain.set_cell(Terrain.TerrainLayer.UIGhosts, GetMousePositionToTerrainPos(), 2, Vector2(0, 2))
	
func DrawOrderingRectangle():
	var mousePos = GetMousePositionToTerrainPos()
	draw_layer.DrawRectAroundTerrainPositions(startPlacingPos, mousePos)
	
	DrawCrosshairAtCursor()
	
func DrawPrototypeInLine():
	var mousePos = GetMousePositionToTerrainPos()
	var diffX = abs(mousePos.x - startPlacingPos.x)
	var diffY = abs(mousePos.y - startPlacingPos.y)
	
	terrain.clear_layer(Terrain.TerrainLayer.UIGhosts)
	
	if diffX > diffY: #Horizontal
		var range = range(startPlacingPos.x, mousePos.x + 1, 1)
		if mousePos.x < startPlacingPos.x:
			range = range(startPlacingPos.x, mousePos.x - 1, -1)
			
		for i in range:
			terrain.set_cell(Terrain.TerrainLayer.UIGhosts, Vector2i(i, startPlacingPos.y), placingPrototype.tileMapIndex, placingPrototype.tileMapPos)
	
	else:
		var range = range(startPlacingPos.y, mousePos.y + 1, 1)
		if mousePos.y < startPlacingPos.y:
			range = range(startPlacingPos.y, mousePos.y - 1, -1)
			
		for i in range:
			terrain.set_cell(Terrain.TerrainLayer.UIGhosts, Vector2i(startPlacingPos.x, i), placingPrototype.tileMapIndex, placingPrototype.tileMapPos)
	
	
	
func GetMousePositionToTerrainPos():
	var x = get_parent().get_global_mouse_position().x / 16
	var y = get_parent().get_global_mouse_position().y / 16
	return Vector2i(x,y)

func BeginPlacing(buildable):
	print("build: " + buildable.get_path())
	placingPrototype = buildable.instantiate()
	ChangeUIMode(UIMode.PLACING)
	terrain.set_layer_modulate(Terrain.TerrainLayer.UIGhosts, Color(1, 1, 1, 0.4))
	CloseMenus()
	
func PlaceOrders():
	var mousePos = GetMousePositionToTerrainPos()
	var targetItems = terrain.FindOrderableItems(startPlacingPos, mousePos, currentOrder)
	for item in targetItems:
		taskManager.AddOrder(currentOrder, item)
	
func BeginOrdering(order):
	print("recieved order: " + str(order))
	currentOrder = order
	ChangeUIMode(UIMode.ORDERING)
	CloseMenus()
	
func CloseMenus():
	for child in get_children():
		child.CloseMenus()
