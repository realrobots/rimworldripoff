extends Node

class_name TaskManager

var taskQueue = []

func RequestTask():
	if len(taskQueue) > 0:
		
		var task = taskQueue[0]
		taskQueue.erase(task)
		
		if task.taskType == Task.TaskType.RequirementDelivery:
			var groupTasks = FindResourceDeliveryTasksWithSameRequirements(task)
			if len(groupTasks) > 0:
				task.GroupSimilarRequirementDeliveries(groupTasks)
				
				for t in groupTasks:
					taskQueue.erase(t)
			
		
		if Time.get_ticks_msec() - task.lastAttemptTime > 2000:
			return task
		else:
			taskQueue.append(task)
		
	return null
	
func FindResourceDeliveryTasksWithSameRequirements(task : Task):
	var list = []
	for t in taskQueue:
		if t.taskType == Task.TaskType.RequirementDelivery and task.GetTargetItemName() == t.GetTargetItemName():
			list.append(t)
	return list
			
	
func RequestFindAndEatFoodTask():
	var task = Task.new()
	task.InitFindAndEatFoodTask()
	return task
	
func ReturnTaskUnfinished(task):
	task.Restart()
	taskQueue.append(task)
	
	if task.groupedTasks != null:
		for t in task.groupedTasks:
			t.Restart()
			taskQueue.append(t)
			
	
func AddBuildOrder(targetItem):
	targetItem.InitRequirements()
	CreateResourceDeliveryTask(targetItem)
	
func CreateResourceDeliveryTask(targetItem):
	for requirement in targetItem.GetRequirements():
		var newTask = Task.new()
		newTask.InitDeliveryTask(targetItem, targetItem.GetRequirement(requirement))
		taskQueue.append(newTask)
	
func ReportItemDeliveredTo(targetItem, item):
	print("DELIVERED")
	print(targetItem.HasAllRequirements())
	if targetItem.HasAllRequirements():
		AddTask(Task.TaskType.Build, targetItem)
	elif targetItem.DoesNeedRequirement(item.name):
		var newTask = Task.new()
		newTask.InitDeliveryTask(targetItem, targetItem.GetRequirement(item.name))
		taskQueue.append(newTask)
	
func AddOrder(order, targetItem):
	match order:
		Task.Orders.Cancel:
			pass
		Task.Orders.Deconstruct:
			pass
		Task.Orders.Mine:
			pass
		Task.Orders.Chop:
			pass
		Task.Orders.Harvest:
			AddTask(Task.TaskType.Harvest, targetItem)
	
func AddTask(taskType, targetItem):
	var newTask = Task.new()
	
	if taskType == Task.TaskType.Harvest:
		newTask.InitHarvestPlantTask(targetItem)
		taskQueue.append(newTask)
	elif taskType == Task.TaskType.Build:
		newTask.InitBuildTask(targetItem)
		taskQueue.append(newTask)
