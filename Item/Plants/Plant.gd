@tool
extends Item

class_name Plant

var harvestProgress : float = 0
var harvestDifficulty : float = 4

@export var harvestItem : String = "Berries"
var harvestAmount : Vector2i = Vector2i(5, 15)

var growth : float = 1
var rot : int = -1
var currentGrowthLevel : int = 0
var lastHarvestTime
@export var harvestGrowthInterval = 5000
var harvestable = false
var despawnDeadTreeInterval = 100

var allowNeighbours = false
var growthAtLastSeed = 0

@export var growthTileMapPos = []
@export var growthIntervals = []
@export var hasFruitTileMapPos : Vector2i
@export var deadTileMapPos : Vector2i



func _init():
	super._init()
	add_to_group("Plant")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func Update(delta):
	var rng = RandomNumberGenerator.new()
	
	
	if len(terrain.GetSurroundingItems(terrain.WorldToTerrainPos(position))) > 0 and not allowNeighbours:
		growth -= delta * 0.5
		if growth <= 0:
			terrain.RemoveItemFromWorld(self)
	else:	
		growth += delta * rng.randf_range(0.7, 1.3) * 1
	
	if rot >= 0:
		rot += growth
		if rot > 10 * 1000:
			terrain.RemoveItemFromWorld(self)
	
	
	if not IsMature():
		if growth > growthIntervals[currentGrowthLevel] * 1000:
			currentGrowthLevel += 1
			terrain.UpdateItemTile(self)
			lastHarvestTime = growth
	else:
		
		if growth > growthIntervals[len(growthIntervals)-1] * 1000 * 4:
			if rng.randi_range(0, despawnDeadTreeInterval * 1000) < delta:
				rot = 0
				terrain.UpdateItemTile(self)
		
		if hasFruitTileMapPos != Vector2i(-1,-1):
			if growth - lastHarvestTime > harvestGrowthInterval * 1000 :
				harvestable = true
				terrain.UpdateItemTile(self)
				
		if growth - growthAtLastSeed > 1000 * 60:
			growthAtLastSeed = growth
			var terrainPos = terrain.WorldToTerrainPos(position)
			var possiblePositions = terrain.GetSpiralPositions(terrainPos, 49)
			
			
			var newTree = terrain.PlaceItemByName(name, possiblePositions[rng.randi_range(1, len(possiblePositions)-1)])
			
			

func IsMature() -> bool:
	return currentGrowthLevel >= len(growthTileMapPos)-1
	
func GetTileMapPos() -> Vector2i:
	if rot >= 0:
		return deadTileMapPos
	elif harvestable:
		return hasFruitTileMapPos
	else:
		return growthTileMapPos[currentGrowthLevel]
		
func RandomizeAge():
	var rng = RandomNumberGenerator.new()
	
	if rng.randi_range(0, 10) <= 5:
		allowNeighbours = true
	
	growth = rng.randi_range(0, growthIntervals[len(growthIntervals)-1]) * 150
	
	for i in range(len(growthIntervals)-1):
		if growth > growthIntervals[i] * 1000:
			currentGrowthLevel = i
			
	terrain.UpdateItemTile(self)
		
	
	
func CanHarvest():
	return harvestable
	
func TryHarvest(amount : float) -> bool:
	harvestProgress += amount * 1/harvestDifficulty
	
	if harvestProgress >= 1:
		harvestProgress = 0
		lastHarvestTime = growth
		harvestable = false
		#itemManager.RemoveItemFromWorld(self)
		#var rng = RandomNumberGenerator.new()
		terrain.PlaceItemByName(harvestItem, terrain.WorldToTerrainPos(position), randi_range(harvestAmount.x, harvestAmount.y))
		terrain.UpdateItemTile(self)
		return true
	else:
		return false
		
func OnClick():
	taskManager.AddTask(Task.TaskType.Harvest, self)

