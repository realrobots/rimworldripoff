@tool
extends Node
class_name Item

@export var tileMapIndex : int
@export var tileMapPos : Vector2i
@export var placingMode : UI.PlacingMode
@export var buildDifficulty : float = 1

@export var count : int
@export var weight : float

@onready var taskManager = $"../../TaskManager"
@onready var itemManager = $"../../ItemManager"

var position : Vector2

var terrain = null

func _init():
	add_to_group("Item")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func GetTileMapPos() -> Vector2i:
	return tileMapPos
	
func CanHarvest():
	return false
	
func GetCount():
	return count
	
func GetName():
	return name
