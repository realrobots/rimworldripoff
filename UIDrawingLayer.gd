extends Node2D

var start : Vector2
var end : Vector2
var line_color = Color.PURPLE
var line_width = 0.5

var tracepoints = []

func DrawRectAroundTerrainPositions(terrainStartPos : Vector2i, terrainEndPos : Vector2i):
	start = terrainStartPos * 16
	end = terrainEndPos * 16
	
	if end.x >= start.x:
		end.x += 16
	else:
		start.x += 16
		
	if end.y >= start.y:
		end.y += 16
	else:
		start.y += 16
	
	
	queue_redraw()
	
func Clear():
	start = Vector2.ZERO
	end = Vector2.ZERO
	queue_redraw()
	
func _draw():
	draw_line(Vector2(start.x, start.y), Vector2(end.x, start.y), line_color, line_width, true)
	draw_line(Vector2(end.x, start.y), Vector2(end.x, end.y), line_color, line_width, true)
	draw_line(Vector2(end.x, end.y), Vector2(start.x, end.y), line_color, line_width, true)
	draw_line(Vector2(start.x, end.y), Vector2(start.x, start.y), line_color, line_width, true)
	
	for i in range(1, len(tracepoints)):
		draw_line(tracepoints[i], tracepoints[i-1], Color.RED, 0.5, true)
	
func trace_points(points):
	tracepoints = points
	queue_redraw()
	
