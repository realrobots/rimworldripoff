
extends TileMap

class_name Terrain

@onready var taskManager = $"../TaskManager"
@onready var pathFinding = $"../Pathfinding"
@onready var itemManager = $"../ItemManager"
@onready var drawLayer = $"../UIDrawingLayer"

enum TerrainLayer {Base = 0, Built = 1, Items = 2, ConstructionGhosts = 3, UIGhosts = 4}

@export var generateTerrain : bool
@export var clearTerrain : bool

@export var mapWidth : int
@export var mapHeight : int

@export var terrainSeed : int

@export var grassThreshold : float
@export var grass2Threshold : float
@export var dirtThreshold : float
@export var rockThreshold : float

var lastPlantUpdate : float = 0

var constructions = {} # {Vector2i:Consturction}
var items = {}
var queuedForDeletion = []

# Called when the node enters the scene tree for the first time.
func _ready():
	set_layer_modulate(Terrain.TerrainLayer.Items, Color(1, 1, 1, 1))
	set_layer_modulate(Terrain.TerrainLayer.ConstructionGhosts, Color(1, 1, 1, 0.4))
	GenerateTerrain()	
	
	
	#PlaceItemByName("Wood", Vector2i(10, 10), 5)
	#PlaceItemByName("Wood", Vector2i(11, 10), 5)
	#PlaceItemByName("Wood", Vector2i(12, 10), 5)
	#PlaceItemByName("Wood", Vector2i(13, 10), 5)
	#PlaceItemByName("Wood", Vector2i(15, 10), 5)
	#PlaceItemByName("Wood", Vector2i(16, 10), 5)
	#PlaceItemByName("Wood", Vector2i(17, 10), 5)
	#PlaceItemByName("Wood", Vector2i(18, 10), 5)
	#PlaceItemByName("Wood", Vector2i(19, 10), 5)
	#PlaceItemByName("Wood", Vector2i(20, 10), 5)
	#PlaceItemByName("Wood", Vector2i(21, 10), 5)
	#PlaceItemByName("Wood", Vector2i(22, 10), 5)
	#PlaceItemByName("Wood", Vector2i(23, 10), 5)
	#PlaceItemByName("Wood", Vector2i(24, 10), 5)
	#PlaceItemByName("Wood", Vector2i(25, 10), 5)
	#PlaceItemByName("Wood", Vector2i(26, 10), 5) # 80 wood total
	#
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if Time.get_ticks_msec() - lastPlantUpdate > 100:
		for item in items:
			if items[item] is Plant:
				items[item].Update((Time.get_ticks_msec() - lastPlantUpdate) * 10)
		
		lastPlantUpdate = Time.get_ticks_msec()
	
	if generateTerrain:
		generateTerrain = false
		GenerateTerrain()
		
	if clearTerrain:
		clearTerrain = false
		clear()
		items.clear()
		constructions.clear()
		
	RemoveQueuedItemsFromWorld()
		
func FindOrderableItems(from : Vector2i, to : Vector2i, order : Task.Orders):
	var itemsInRect = GetItemsInRect(from, to)
	var targetItems = []
	for item in itemsInRect:
		match order:
			Task.Orders.Cancel:
				pass
			Task.Orders.Deconstruct:
				pass
			Task.Orders.Mine:
				pass
			Task.Orders.Chop:
				pass
			Task.Orders.Harvest:
				if item.CanHarvest():
					targetItems.append(item)
	return targetItems
	
func GetSurroundingItems(terrainPos : Vector2i):
	var surroundingPos = []
	surroundingPos.append(terrainPos + Vector2i(1,1))
	surroundingPos.append(terrainPos + Vector2i(0,1))
	surroundingPos.append(terrainPos + Vector2i(-1,1))
	surroundingPos.append(terrainPos + Vector2i(1,0))
	surroundingPos.append(terrainPos + Vector2i(-1,0))
	surroundingPos.append(terrainPos + Vector2i(1,-1))
	surroundingPos.append(terrainPos + Vector2i(0,-1))
	surroundingPos.append(terrainPos + Vector2i(-1,-1))
	
	var surroundingItems = []
	for i in range(len(surroundingPos)):
		if items.has(surroundingPos[i]):
			surroundingItems.append(items[surroundingPos[i]])
			
	return surroundingItems
	
func GetItemsInRect(from : Vector2i, to : Vector2i):
	var list = []
	for x in range(min(from.x, to.x), max(from.x, to.x)+1):
		for y in range(min(from.y, to.y), max(from.y, to.y)+1):
			if items.has(Vector2i(x, y)):
				list.append(items[Vector2i(x,y)])
	return list
	
func GetItemAtPosition(terrainPos : Vector2i):
	if items.has(terrainPos):
		return items[terrainPos]
	elif constructions.has(terrainPos):
		return constructions[terrainPos]
		
	return null
	
func UpdateItemTile(item):
	set_cell(TerrainLayer.Items, WorldToTerrainPos(item.position), item.tileMapIndex, item.GetTileMapPos())
		
func PlaceItemByName(itemName : String, terrainPos : Vector2i, amount : int = 1):
	var prototype = itemManager.FindItemPrototype(itemName)
	var item = prototype.duplicate()
	item.count = amount
	PlaceItem(item, terrainPos)
	
	
func PlaceItem(item, terrainPos : Vector2i):
	var layer = null
	
	if not IsInRange(terrainPos):
		return null
	
	var positions = GetSpiralPositions(terrainPos, 25)
	
	for pos in positions:
		if IsPositionEmpty(pos):	
			if item is Item:
				layer = TerrainLayer.Items
				items[pos] = item
			elif item is Wall:
				layer = TerrainLayer.Built
				constructions[pos] = item	
			
			item.position = TerrainToWorldPos(pos)
			item.terrain = self
			if item is Plant:
				item.RandomizeAge()
			
			set_cell(layer, pos, item.tileMapIndex, item.GetTileMapPos())
			#print("placing item " + item.name)
			break
			
func IsInRange(pos : Vector2i) -> bool:
	if pos.x < 0 or pos.y < 0 or pos.x >= mapWidth or pos.y >= mapHeight:
		return false
	else:
		return true
			
func RemoveItemFromWorld(item, amount : int = 0):
	if amount != 0:
		item.count -= amount
	
	if amount == 0 or item.count <= 0:	
		queuedForDeletion.append(item)
	
	
func RemoveQueuedItemsFromWorld():
	for item in queuedForDeletion:
		var layer
		var terrainPos = WorldToTerrainPos(item.position)
		if item is Item:
			layer = TerrainLayer.Items
			items.erase(terrainPos)			
		elif item is Wall:
			layer = TerrainLayer.Built
			constructions.erase(terrainPos)			
		set_cell(layer, terrainPos, -1)
	queuedForDeletion = []
		
			
func FindNearestItem(itemCategory : String, worldPosition : Vector2):
	print("requirest " + str(itemCategory))
	if len(items) == 0:
		return null
		
	var nearestItem = null
	var nearestDistance = 999999
	
	for key in items:
		var item = items[key]
		
		
		if IsItemInCategory(item, itemCategory):
			var distance = worldPosition.distance_to(item.position)
			
			if nearestItem == null:
				nearestItem = item
				nearestDistance = distance
				continue
				
			if distance < nearestDistance:				
				nearestItem = item
				nearestDistance = distance
				
	return nearestItem
	
func IsItemInCategory(item, itemCategory) -> bool:
	return item.is_in_group(itemCategory) or item.name == itemCategory

func GetSpiralPositions(origin : Vector2i, max_count : int):
	var x = 0
	var y = 0
	var positions = []
	for i in range(max_count):
		positions.append(Vector2i(origin.x + x, origin.y + y))
		if(abs(x) <= abs(y) && (x != y || x >= 0)):
			if y >= 0:
				x += 1
			else:
				x += -1
		else:
			if x >= 0:
				y += -1
			else:
				y += 1
	return positions
	
	
func IsPositionEmpty(terrainPos : Vector2i):
	if not items.has(terrainPos) and not constructions.has(terrainPos):
		return true
	else:
		return false
	
func PlaceConstructionOrder(placingPrototype, terrainPos):
	if constructions.has(terrainPos):
		print("consturction order already exists at this position: " + str(terrainPos))
		return
		
	var newConstruction = placingPrototype.duplicate()
	constructions[terrainPos] = newConstruction
	newConstruction.position = TerrainToWorldPos(terrainPos)
	newConstruction.terrain = self
	taskManager.AddBuildOrder(newConstruction)
	set_cell(TerrainLayer.ConstructionGhosts, terrainPos, newConstruction.tileMapIndex, newConstruction.GetTileMapPos())
	
func OnConstructionComplete(construction):
	erase_cell(TerrainLayer.ConstructionGhosts, WorldToTerrainPos(construction.position))
	set_cell(TerrainLayer.Built, WorldToTerrainPos(construction.position), construction.tileMapIndex, construction.GetTileMapPos())
	pathFinding.AddConstructionToPathfinding(TerrainLayer.Built, WorldToTerrainPos(construction.position))
	
func GenerateTerrain():	
	print("Generating Terrain...")
	clear()
	items.clear()
	constructions.clear()
	var noise = FastNoiseLite.new()
	noise.noise_type = FastNoiseLite.TYPE_CELLULAR
	
	
	var rng = RandomNumberGenerator.new()
	
	if terrainSeed == 0:
		noise.seed = rng.randi()
	else:
		noise.seed = terrainSeed
	
	
	for x in range(mapWidth):
		for y in range(mapHeight):
			if noise.get_noise_2d(x, y) > grassThreshold:
				set_cell(0, Vector2i(x, y), 0, Vector2i(0,0))
				if rng.randi_range(0, 20) == 0:
					if randi_range(0, 2) == 0:
						#PlaceItemByName("Tree", Vector2i(x, y))
						pass
					else:						
						#PlaceItemByName("Pine Tree", Vector2i(x, y))
						pass
					pass
				elif rng.randi_range(0, 50) == 0:					
					#PlaceItemByName("Berry Bush", Vector2i(x, y))
					pass
			elif noise.get_noise_2d(x, y) > grass2Threshold:
				set_cell(0, Vector2i(x, y), 0, Vector2i(1,0))
				if randi_range(0, 40) == 0:
					#PlaceItemByName("Pine Tree", Vector2i(x, y))
					pass
			elif noise.get_noise_2d(x, y) > dirtThreshold:
				set_cell(0, Vector2i(x, y), 0, Vector2i(2,0))				
				if randi_range(0, 100) == 0:
					#PlaceItemByName("Pine Tree", Vector2i(x, y))
					pass
			elif noise.get_noise_2d(x, y) > rockThreshold:
				set_cell(0, Vector2i(x, y), 0, Vector2i(3,0))
			else: 
				set_cell(0, Vector2i(x, y), 0, Vector2i(0,1))
				
func WorldToTerrainPos(worldPos : Vector2) -> Vector2i:
	var x = worldPos.x / tile_set.tile_size.x
	var y = worldPos.y / tile_set.tile_size.y
	return Vector2(x,y)
	
func TerrainToWorldPos(terrainPos : Vector2i) -> Vector2:
	var x = terrainPos.x * tile_set.tile_size.x
	var y = terrainPos.y * tile_set.tile_size.y
	return Vector2(x,y)

