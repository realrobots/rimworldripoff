extends Node

@onready var taskManager = $"../../TaskManager"
@onready var itemManager = $"../../ItemManager"
@onready var terrain = $"../../Terrain"

@onready var charController = $".."

@onready var hungerBar = $"../hungerBar"

enum PawnAction {Idle, DoingSubTask}

var currentAction : PawnAction = PawnAction.Idle

var currentTask : Task = null

var foodNeed : float = 1 #0 =min, 1=max
var eatSpeed : float = 0.5
var foodNeedDepleteSpeed : float = 0.001

var harvestSkill : float = 1
var buildSkill : float = 1

var inHand;

func _process(delta):
	#foodNeed -= foodNeedDepleteSpeed * delta
	
	
	if currentTask != null:
		DoCurrentTask(delta)
	else:
		if foodNeed < 0.5:
			currentTask = taskManager.RequestFindAndEatFoodTask()
		else:
			currentTask = taskManager.RequestTask()
		
func OnPickupItem(item, amount):
	var inHandTemp = item.duplicate()
	
	
	if item.count < amount:
		amount = inHandTemp.count
	inHandTemp.count = amount
	
	if inHand != null:
		if inHand.name == inHandTemp.name:
			inHand.count += inHandTemp.count
		else:
			print("ERROR: did you just pick up " + inHandTemp.name + " while you were already carrying " + inHand.name + "?")
	else:
		inHand = inHandTemp
		
	print("picked up " + str(amount) + " wood, now carrying " + str(inHand.count) + " wood")
	terrain.RemoveItemFromWorld(item, amount)
		
func DeliverRequirement(target):
	if inHand == null:
		print("ERROR: cannot deliver, no item in hand")
		return
		
	var dropAmount = currentTask.GetDeliveryAmount()
	if inHand.count < dropAmount:
		dropAmount = inHand.count
		
	target.DeliverRequirement(inHand.name, dropAmount)
	taskManager.ReportItemDeliveredTo(target, inHand)
	inHand.count -= dropAmount
	
	print("dropped off " + str(dropAmount) + " wood, now carrying " + str(inHand.count) + " wood")
	
	if inHand.count <= 0:
		inHand = null

func OnFinishedSubTask():
	currentAction = PawnAction.Idle
	
	if currentTask.IsFinished():
		if currentTask.groupedTasks != null:
			currentTask = currentTask.GetNextTaskInGroup()
			print("Got next task in group")
		else:		
			currentTask = null
		
func DoCurrentTask(delta):
	var subTask = currentTask.GetCurrentSubTask()
	
	if currentAction == PawnAction.Idle:
		StartCurrentSubTask(subTask)
	else:
		match subTask.taskType:
			Task.TaskType.WalkTo:
				if charController.HasReachedDestination():
					currentTask.OnReachedDestination()
					OnFinishedSubTask();
					
			Task.TaskType.WalkNextTo:
				if charController.HasReachedDestination():
					currentTask.OnReachedDestination()
					OnFinishedSubTask();
					
			Task.TaskType.Eat:
				if inHand.nutrition > 0 and foodNeed < 1:
					inHand.nutrition -= eatSpeed * delta
					foodNeed += eatSpeed * delta
				else:
					print("finished eating food")
					inHand = null
					
					currentTask.OnFinishSubTask()
					OnFinishedSubTask()
					
			Task.TaskType.Harvest:
				var targetItem = currentTask.GetCurrentSubTask().targetItem
				if targetItem.TryHarvest(harvestSkill * delta):
					currentTask.OnFinishSubTask()
					OnFinishedSubTask()
				else:
					print(targetItem.harvestProgress)
					
			Task.TaskType.Build:
				var targetItem = currentTask.GetCurrentSubTask().targetItem
				if targetItem.TryBuild(buildSkill * delta):
					currentTask.OnFinishSubTask()
					OnFinishedSubTask()
				else:
					pass
					#print(targetItem.buildProgress)
					
func HasItemInHand(itemName : String) -> bool:
	if inHand != null:
		if inHand.name == itemName:
			return true
	return false
	
func StartCurrentSubTask(subTask):
	print ("Starting subtask: " + Task.TaskType.keys()[subTask.taskType])
	
	match subTask.taskType:
		Task.TaskType.FindItem:
			if HasItemInHand(subTask.targetItemType):
				print("Hey, I'm already holding some " + subTask.targetItemType)
				currentTask.SkipAheadToSubTask(Task.TaskType.WalkNextTo)
				return
			
			
			var targetItem = terrain.FindNearestItem(subTask.targetItemType, charController.position)
			if targetItem == null:
				print("no item, force task to finish")
				taskManager.ReturnTaskUnfinished(currentTask)
				currentTask = null
				currentAction = PawnAction.Idle
				return
			else:
				currentTask.OnFoundItem(targetItem)
				
			OnFinishedSubTask()
			
		Task.TaskType.WalkTo:
			charController.SetMoveTarget(subTask.targetItem.position)
			currentAction = PawnAction.DoingSubTask
			
		Task.TaskType.WalkNextTo:
			charController.SetMoveTarget(subTask.targetItem.position, true)
			currentAction = PawnAction.DoingSubTask
			
		Task.TaskType.Pickup:
			OnPickupItem(subTask.targetItem, subTask.targetItemAmount)
			
			if inHand.count < subTask.targetItemAmount:				
				var targetItem = terrain.FindNearestItem(inHand.name, charController.position) 
				
				if targetItem != null:
					currentTask.GoBackToSubTask(Task.TaskType.FindItem)
					currentTask.OnFoundItem(targetItem)
				else:
					currentTask.OnFinishSubTask()
			else:			
				currentTask.OnFinishSubTask()
			OnFinishedSubTask()
			
		Task.TaskType.Eat:
			currentAction = PawnAction.DoingSubTask
			
		Task.TaskType.Harvest:
			currentAction = PawnAction.DoingSubTask
			
		Task.TaskType.Build:
			currentAction = PawnAction.DoingSubTask
			
		Task.TaskType.RequirementDelivery:
			DeliverRequirement(subTask.targetItem)
			currentTask.OnFinishSubTask()
			OnFinishedSubTask()
